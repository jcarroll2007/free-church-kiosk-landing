import 'angular';

import component from './contact-form.component.js';

import './contact-form.scss';

export default angular
	.module('contact-form', [])
	.component('contactForm', component)
	.name;
import template from './contact-form.html';
import component from './contact-form.controller.js';

export default {
	template,
	component
}